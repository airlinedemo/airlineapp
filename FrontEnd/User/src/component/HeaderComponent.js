import React, { Component } from 'react';

class HeaderComponent extends Component {
    render() {
        return (
            <div>
                <header>
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                     <div>
                      <a href="http://www.google.com/" className="navbar-brand" style={{marginLeft:"20px", color: "#F2EDD7"}}>Airline Reservation System</a> 
                     </div>
                    </nav>
                </header>
            </div>
        );
    }
}

export default HeaderComponent;