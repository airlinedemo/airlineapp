import React, { Component } from 'react';
import UpdateProfileService from '../Services/UpdateProfileService';

class UpdateInfo extends Component {
    constructor(props){
        super(props);
        
        this.state = {
                           date_Of_Birth:"",
                           mobile_No:"",
                           password:"",
                           city:"",
                           pincode:""
                 }
            
                 this.changeDOBHandler=this.changeDOBHandler.bind(this);
                 this.changeMobileHandler=this.changeMobileHandler.bind(this);
                 this.changePasswordHandler=this.changePasswordHandler.bind(this);
                 this.changeCityHandler=this.changeCityHandler.bind(this);
                 this.changePincodeHandler=this.changePincodeHandler.bind(this);
                 this.updatedProfile=this.updatedProfile.bind(this);
    
                }
            updatedProfile=(e)=>{
            e.preventDefault();
    
            let modifiedCust = {
                date_Of_Birth:this.state.date_Of_Birth,
                mobile_No:this.state.mobile_No,
                password:this.state.password,
                city:this.state.city,
                pincode:this.state.pincode
            };
            
            UpdateProfileService.updatedProfile(sessionStorage.getItem("loginEmail"),modifiedCust).then(res=>{

            alert("Profile Updated, login again")     
            this.props.history.push('/signin')
            });
    
        }

        changeDOBHandler=(event)=>{
            this.setState({date_Of_Birth: event.target.value})
        }
    

        changeMobileHandler=(event)=>{
            this.setState({mobile_No: event.target.value})
        }
    

    
        changePasswordHandler=(event)=>{
            this.setState({password: event.target.value})
        }
    
    
        changeCityHandler=(event)=>{
            this.setState({city: event.target.value})
        }
    
    
        changePincodeHandler=(event)=>{
            this.setState({pincode: event.target.value})
        }
    
    render() {
        return (
            <div>
            <div className="card col-md-3 offset-md-4" style={{marginTop: "10px", backgroundColor: "#1c283b"}}>
            <h2 className="text-color">Update Information</h2>

           <div className="card-body">
  <form>          
  
                   <div className="form-group">
                       <input type="date" required="required" placeholder="Enter Date of Birth" name="date_Of_Birth" className="form-control" value={this.state.date_Of_Birth} onChange={this.changeDOBHandler}/>
                   </div>

                   <div className="form-group">
                       <input type="number" required="required" placeholder="Enter Mobile Number" name="mobile_No" className="form-control" value={this.state.mobile_No} onChange={this.changeMobileHandler}/>
                   </div>
   
                   <div className="form-group">
                       <input type="text" required="required" placeholder="Enter Password" name="password" className="form-control" value={this.state.password} onChange={this.changePasswordHandler} />
                   </div>
                   <div className="form-group">
                       <input type="text" required="required" placeholder="Enter City Name" name="city" className="form-control" value={this.state.city} onChange={this.changeCityHandler} />
                   </div>
 

                   <div className="form-group">
                       <input type="number" required="required" placeholder="Enter Pincode" name="pincode" className="form-control" value={this.state.pincode} onChange={this.changePincodeHandler} />
                   </div>
                   <input type="button" value="Update" className="btn btn-success" onClick={this.updatedProfile}/> 
          
</form>
</div>
</div>
</div>
        );
    }
}

export default UpdateInfo;