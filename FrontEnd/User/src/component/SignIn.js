import React, { Component } from 'react';
import CustomerLoginService from '../Services/CustomerLoginService';



class SignIn extends Component {

    constructor(props) {
        super(props);
        
        this.state={
            email:"",
            password:""
        }
        this.changeEmailHandler = this.changeEmailHandler.bind(this);
        this.changePasswordHandler = this.changePasswordHandler.bind(this)
        this.loginCustomer = this.loginCustomer.bind(this);
    }
    
    loginCustomer=(e)=>{
        e.preventDefault();

        let login = {
            email:this.state.email,
            password:this.state.password
        };

        //console.log("login=>"+JSON.stringify(login));
       CustomerLoginService.loginCustomer(login).then(res=>{
        sessionStorage.setItem("loginEmail",res.data.id)
        sessionStorage.setItem("CustName",res.data.first_Name)
        console.log(res.data.id);
       
        this.props.history.push('/customers') 
     });
    }

    changeEmailHandler=(event)=>{
        this.setState({email: event.target.value});
      
    }

    changePasswordHandler=(event)=>{
        this.setState({password: event.target.value});
    }
    RegisterCustomer=(e)=>{
        this.props.history.push("/customerregister")
    }

    render() {
        return (
            <div>
               <div className="container-md bg-img">
                   <div className="row">
                      <div className="card col-md-3 offset-md-1 , box-format" style={{marginTop: "20px" , border: 0 }}>
                       
                         <h2 className="text-color">Customer Login</h2> 
                         
                         <div className="card-body">
                             
                             <form>
                                 <div className="form-group">
                                     <input type="email" required="required" placeholder="Enter Email" name="email" className="form-control"
                                     value={this.state.email} onChange={this.changeEmailHandler}/>
                                 </div><br/>

                                 <div className="form-group">
                                     <input type="password" required="required" placeholder="Enter Password" name="password" className="form-control"
                                     value={this.state.password} onChange={this.changePasswordHandler}/>
                                 </div>
                                 <div >
                                    {/* <a className="password-color" href="/forgetpassword">Forget Password ?</a> */}
                                </div><br/>
                                 <table>
                                 <td width="50%">    
                                 <input type="submit" value="Sign In" className="btn btn-success" onClick={this.loginCustomer}/>
                                 </td>
                                 <td width="50%">    
                                 <input type="button" value="Register" className="btn btn-warning" onClick={this.RegisterCustomer}/>
                                 </td>
                                 </table>
                             </form>
                         </div> 
                      </div>       
                   </div>
               </div>
            </div>
        );
    }
}

export default SignIn;