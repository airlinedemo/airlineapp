import React, { Component} from 'react';

class CustomerPage extends Component {

    render() {
        return (
            <div className="cust-bag cust-op">
            <br />
            <h4>Welcome {sessionStorage.getItem("CustName")}</h4>
            <div className="bg-cover">
            <h1 className="txt-shadow" style={{color:'white' ,textShadow:2}}>Lets Fly Away In Style!</h1>
            <br />
            <br />
            <br />
            <div>
            <table >
            <tr>
                <td >
                <div class="dropdown" style={{marginLeft:200}} >
                   <button class="dropbtn" style={{marginLeft:100}}>Search Flights </button>
                   <div class="dropdown-content">
                     <a className="btn btn-info" href="/searchflightbysrcdest"> By Source & Destination</a>
                    
                   </div>
                  </div>
                </td>
                <td >
                <div class="dropdown" style={{marginLeft:200}}>
                   <button class="dropbtn" style={{marginLeft:100}}>Display Details </button>
                   <div class="dropdown-content">
                     <a className="btn btn-secondary" href="/displayallflights"> All Flights</a>
                     <a className="btn btn-warning" href="/displaybooking"> Booking Details</a>
                     <a className="btn btn-info" href="/searchFlightById"> Search Flight Details</a>
                   </div>
                  </div>
                </td>
                <td >
                <div class="dropdown" style={{marginLeft:200}}>
                   <button class="dropbtn" style={{marginLeft:100}}>Customer Info </button>
                   <div class="dropdown-content">
                    <a className="btn btn-info" href="/showcustomeraccount">Show Profile </a>
                     <a className="btn btn-danger" href="/deletecustomeraccount"> Delete Account </a>
                     
                   </div>
                  </div>
                </td>
               
            </tr>    
           
            </table>
            </div>
            </div>
        </div>   
        );
    }
}

export default CustomerPage;