import React, { Component } from 'react';

class SignOut extends Component {
 constructor(props){
 super(props);
 this.noLogoutCustomer=this.noLogoutCustomer.bind(this);
 this.exitCustomer=this.exitCustomer.bind(this);
 }
    noLogoutCustomer(){
        this.props.history.push("/customers")
    }


    exitCustomer(){
        sessionStorage.clear();
        //alert(sessionStorage.getItem("loginEmail"))
        this.props.history.push("/signin")
    }
    render() {
        return (
            <div>
               <div className="container-md bg-img">
                   <div className="row">
                      <div className="card col-md-4 offset-md-4" style={{marginTop: "20px" , border: 0 }}>
                       <br/>
                         <h2 className="text-center">Confirm Exit..!!!</h2> 
                         <hr/>
                         <div className="card-body">
                             
                             <form>
                                 <div className="form-group">
                                    <h5>Are you sure, you want to logout?</h5>
                                 </div><br/>
                                 <table>
                                 <td width="70%"></td>    
                                 <td width="20%">    
                                 <input type="button" value="No" className="btn btn-link" style={{color:'red'}} onClick={this.noLogoutCustomer}/>
                                 </td>
                                 <td width="10%">    
                                 <input type="submit" value="Yes" className="btn btn-link" style={{color:'red'}} onClick={this.exitCustomer}/>
                                 </td>
                                 
                                 </table>
                             </form>
                         </div> 
                      </div>       
                   </div>
               </div>
            </div>
        );
    }
}

export default SignOut;