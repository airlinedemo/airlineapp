import React, { Component } from 'react';
import DeleteCustomerService from '../Services/DeleteCustomerService';
;

class DeleteAccount extends Component {
    constructor(props){
        super(props);
        this.state={
            id:""
        }
       
        this.nodeleteCustomer=this.nodeleteCustomer.bind(this);
        this.deleteCustomer=this.deleteCustomer.bind(this);
        }
    
        nodeleteCustomer(){
               this.props.history.push("/customers")
           }
                    
                
                
        
       
        deleteCustomer=(e)=>{
        e.preventDefault();
        let cust ={
            id:sessionStorage.getItem("loginEmail")
        }
        alert(cust.id)
            DeleteCustomerService.deleteCustomerById(cust.id).then(res=>{
                 
                //alert("deleted successfully");
                this.props.history.push("/signin")
            });
        
    }      
           
    
    render() {
        return (
            <div>
               <div className="container-md bg-img">
                   <div className="row">
                      <div className="card col-md-4 offset-md-4" style={{marginTop: "20px" , border: 0 }}>
                       <br/>
                         <h2 className="text-center">Confirm Exit..!!!</h2> 
                         <hr/>
                         <div className="card-body">
                             
                             <form>
                                 <div className="form-group">
                                    <h5>Are you sure, you want to delete your account?</h5>
                                 </div><br/>
                                 <table>
                                 <td width="70%"></td>    
                                 <td width="20%">    
                                 <input type="button" value="No" className="btn btn-link" style={{color:'red'}} onClick={this.nodeleteCustomer}/>
                                 </td>
                                 <td width="10%">    
                                 <input type="submit" value="Yes" className="btn btn-link" style={{color:'red'}} onClick={this.deleteCustomer}/>
                                 </td>
                                 
                                 </table>
                             </form>
                         </div> 
                      </div>       
                   </div>
               </div>
            </div>
        );
    }
}

export default DeleteAccount;