import React, { Component } from 'react';
import SearchBySrcDestService from '../Services/SearchBySrcDestService';


class HomePage extends Component {
     
    constructor(props){
     super(props);
     
     this.state ={
             source : '',
             destination :''
     }

     this.onChangeSourceHandler=this.onChangeSourceHandler.bind(this);
     this.onChangeDestinationHandler=this.onChangeDestinationHandler.bind(this);
     this.SearchBySrcDest=this.SearchBySrcDest.bind(this);
    }

    onChangeSourceHandler=(event)=>{
        
        this.setState({source: event.target.value})

       }
       
       onChangeDestinationHandler=(event)=>{
        
        this.setState({destination:event.target.value})
       }

    SearchBySrcDest=(e)=>{
        
        e.preventDefault();
       
        let src ={source:this.state.source,
                 destination:this.state.destination}
        
        //if( src.source!=="" && src.destination!=="")
        this.props.history.push(`displayflightdetailbysrcdest/`+this.state.source +`/` + this.state.destination);

    SearchBySrcDestService.searchBySrcDest(src).then(res=>{
       console.log(res.data)
    })
     
    };

   

    render() {
        return (
            <div className="home-page">
                <br/>
                <div className="welcome-text" >
                <h1>Welcome to Airline Reservation System</h1>
                <div className="container-md ">
                <div className="row">
                   <div className="card col-md-3 offset-md-9 box-format">
                                       
                      <div className="card-body">
                          <h3>Search Flight</h3>
                          <form>
                              <div className="form-group">
                                  <input type="text" required="required" placeholder="Select Source" name="email" className="form-control" onChange={this.onChangeSourceHandler}/><br/>
                              </div>

                              <div className="form-group">
                                  <input type="text" required="required" placeholder="Select Destination" name="password" className="form-control" onChange={this.onChangeDestinationHandler}/>
                              </div>

                              {/* <div className="form-group">
                                  <input type="date" required="required"  name="date" className="form-control"    />
                              </div> */}
                              <table>
                              <td width="50%">    
                              <input type="button" value="Search" className="btn btn-warning" onClick={this.SearchBySrcDest}/>
                              </td>
                              </table>
                          </form>
                      </div> 
                   </div>       
                </div>
            </div>
                </div>
            </div>
        );
    }
}

export default HomePage;