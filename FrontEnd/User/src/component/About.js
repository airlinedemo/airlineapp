import React, { Component } from 'react';

class About extends Component {
    render() {
        return (
            <div>
                <h2 className="text-center"><u>About us</u></h2>
                <br/>
                <table style={{marginLeft:100}}>
                    <tr >
                        <td>
                        <img  src='https://st2.depositphotos.com/1001877/7590/i/950/depositphotos_75905167-stock-photo-online-booking-flight-or-travel.jpg' width="650px" height="500px" alt="location" class="img-responsive " />
                        </td>
                        <td >
                        <p style={{fontSize:24 ,marginRight:50, textAlign:'justify'}}>
                         The Airline Reservation System project is an implementation of a general Airline Ticketing website, which 
                         helps the customers to search the availability and prices of various airline tickets, and book tickets   
                         accordingly with the reservations. This project also covers various features like online registration of  
                         the users, modifying the details of the website by the management staff or administrator of the website,
                         by adding, deleting or modifying the customer details.
                         </p>
                        </td>
                    </tr>
                </table>
                
            </div>
        );
    }
}

export default About;