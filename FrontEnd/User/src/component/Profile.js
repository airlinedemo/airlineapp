import React, { Component } from 'react';
import ProfileCustomerService from '../Services/ProfileCustomerService';

class Profile extends Component {
 constructor(props) {
        super(props);
        
        this.state={
            id:"",
            customer:[]
        }
        this.showProfile=this.showProfile.bind(this);
        this.updateCustomer=this.updateCustomer.bind(this);
       }


       updateCustomer(){
        this.props.history.push("/updatecustomerinfo")
       }

       showProfile=(e)=>{
            e.preventDefault();
            let cust ={
                id:sessionStorage.getItem("loginEmail")
            }
                ProfileCustomerService.custProfile(cust.id).then(res=>{
                    console.log(res.data);
                     this.setState({customer:res.data})
                    //alert("deleted successfully");
                  
                });
            
    }   




    render() {
        return (
            <div>
               <div className="container-md cust-bag">
                   <div >
                      <div style={{marginTop: "20px" , border: 0 }}>
                       <br/>
                         <h2 className="text-center">Show Profile</h2> 
                         <div className="card-body">
                            

                          <table className="table">
                              <thead>
                                      <td> first_Name</td>
                                      <td> last_Name</td>
                                      <td> date_Of_Birth </td>
                                      <td> gender</td>
                                      <td> mobile_No </td>
                                      <td> password</td>
                                      <td> email</td>
                                      <td> city</td>
                                      <td> pincode</td>
                              </thead>
                              <tbody>
                                   <tr key={this.state.customer.id}>
                                      <td> {this.state.customer.first_Name} </td>
                                      <td> {this.state.customer.last_Name} </td>
                                      <td> {this.state.customer.date_Of_Birth} </td>
                                      <td> {this.state.customer.gender} </td>
                                      <td> {this.state.customer.mobile_No} </td>
                                      <td> {this.state.customer.email} </td>
                                      <td> {this.state.customer.password} </td>
                                      <td> {this.state.customer.city} </td>
                                      <td> {this.state.customer.pincode} </td>
                                    </tr>  
                                    </tbody>  
                             </table>
                             
                            
                                 <input type="button" value="Show Profile" className="btn btn-link" style={{color:'red'}} onClick={this.showProfile}/>
                                
                                    
                                 <input type="submit" value="Update Profile" className="btn btn-link" style={{color:'red'}} onClick={this.updateCustomer}/>
                                
                            
                         </div> 
                      </div>       
                   </div>
               </div>
            </div>
        );
    }
}

export default Profile;