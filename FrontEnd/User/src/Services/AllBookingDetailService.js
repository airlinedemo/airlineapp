import axios from 'axios'

const URL = "http://localhost:8080/user-api/booking/searchById/"

class AllBookingDetails {
    
    getAllBookingDetails(id){
        return axios.get(URL+id);
    }
}

export default new AllBookingDetails();