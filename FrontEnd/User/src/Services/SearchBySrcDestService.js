import axios from 'axios';

//const URL="http://localhost:8080/user-api/flight/searchbysourcedestination/"

class SearchBySrcDestService{
    
searchBySrcDest(source,destination){
    
    return axios.get(`http://localhost:8080/user-api/flight/searchbysourcedestination/${source}/${destination}`);
   // return axios.get(`http://localhost:8080/user-api/flight/searchbysourcedestination/mumbai/delhi`);

}

}

export default new SearchBySrcDestService();