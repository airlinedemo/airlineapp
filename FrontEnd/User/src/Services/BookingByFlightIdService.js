import axios from 'axios'

const URL = "http://localhost:8080/user-api/booking/"
class BookingByFlightid{
   
    bookFlightByCustId(newBooking){
       return axios.post(URL,newBooking)
    }
}

export default new BookingByFlightid();