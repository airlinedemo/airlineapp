import axios from 'axios';

const URL ="http://localhost:8080/user-api/loginreg/updatedetails/"


class UpdateProfileService {

    updatedProfile(id,obj){
        return axios.put(URL+id,obj)
    }

}

export default new UpdateProfileService();