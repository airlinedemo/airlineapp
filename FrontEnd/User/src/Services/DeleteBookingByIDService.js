import axios from 'axios'

const URL="http://localhost:8080/user-api/booking/delete/"
class DeleteBookingByIdService {

    deleteBookingSlotById(id){
        return axios.put(URL+id);
    }
}

export default new DeleteBookingByIdService();