import axios from 'axios';

const URL ="http://localhost:8080/user-api/loginreg/loginCustomer"

class CustomerLoginService{

    loginCustomer(login){
        return axios.post(URL, login);
    }

}
export default new CustomerLoginService();
