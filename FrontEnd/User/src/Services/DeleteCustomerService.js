import axios from 'axios'

const URL=" http://localhost:8080/user-api/loginreg/deletebycustomerid/"
class DeleteCustomerService{
   
    deleteCustomerById(id){
        return axios.delete(URL+id);
    }
}

export default new DeleteCustomerService();