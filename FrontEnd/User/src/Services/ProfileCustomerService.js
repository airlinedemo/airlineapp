import axios from 'axios'

const URL="http://localhost:8080/user-api/loginreg/searchById/"
class ProfileCustomerService {
  custProfile(id){
      return axios.get(URL+id);
  }
}

export default new ProfileCustomerService();