import axios from 'axios'

const URL="http://localhost:8080/user-api/flight/searchByFlightId/"
class SearchFlightByIdService {

    searchFlightByFlightId(fid){
        return axios.get(URL + fid)
    }

}

export default new SearchFlightByIdService();