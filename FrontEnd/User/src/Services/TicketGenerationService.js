import axios from 'axios'

const URL="http://localhost:8080/user-api/booking/searchByBookingId/"
class TicketGenerationService{

    getDetailsBYBookingId(id){
        return axios.get(URL+id);
    }


}

export default new TicketGenerationService();