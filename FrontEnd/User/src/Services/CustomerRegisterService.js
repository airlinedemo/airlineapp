import axios from 'axios'

const URL="http://localhost:8080/user-api/loginreg/customer"
class CustomerRegisterService{
    
 registerCustomer(newCust){
     return axios.post(URL, newCust);
 }

}
export default new CustomerRegisterService();