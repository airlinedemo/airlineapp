import React, { Component } from 'react';
import SearchBySrcDestService from '../Services/SearchBySrcDestService';

class SearchBySourceAndDest extends Component {
    constructor(props){
        super(props);
        
        this.state ={
                source : '',
                destination :''
        }
   
        this.onChangeSourceHandler=this.onChangeSourceHandler.bind(this);
        this.onChangeDestinationHandler=this.onChangeDestinationHandler.bind(this);
        this.SearchBySrcAndDest=this.SearchBySrcAndDest.bind(this);
       }
   
       onChangeSourceHandler=(event)=>{
           
           this.setState({source: event.target.value})
   
          }
          
          onChangeDestinationHandler=(event)=>{
           
           this.setState({destination:event.target.value})
          }
   
          SearchBySrcAndDest=(e)=>{
           
           e.preventDefault();
          
           let src ={source:this.state.source,
                    destination:this.state.destination}
           
           //if( src.source!=="" && src.destination!=="")
           this.props.history.push(`displayquick/`+this.state.source +`/` + this.state.destination);
   
       SearchBySrcDestService.searchBySrcDest(src).then(res=>{
          console.log(res.data)
       })
        
       };
   

    render() {
        return (
            <div>
                
                <br/>
            <div className="card col-md-3 offset-md-4 box-format">
                        <h2  className="text-center">Search Flight</h2>
                        <br/>
                          <form>
                              <div className="form-group">
                                  <input type="text" required="required" placeholder="Select Source" name="email" className="form-control" onChange={this.onChangeSourceHandler}/><br/>
                              </div>

                              <div className="form-group">
                                  <input type="text" required="required" placeholder="Select Destination" name="password" className="form-control" onChange={this.onChangeDestinationHandler}/>
                              </div>

                              {/* <div className="form-group">
                                  <input type="date" required="required"  name="date" className="form-control"    />
                              </div> */}
                              <table>
                              <td width="50%">    
                              <input type="button" value="Search" className="btn btn-warning" onClick={this.SearchBySrcAndDest}/>
                              </td>
                              </table>
                          </form>
            </div>
            </div>
        );
    }
}

export default SearchBySourceAndDest;