import React, { Component } from 'react';


class SearchFlightById extends Component {

    constructor(props){
        super(props);
        
        this.state = {
                       flight_Id :""
                     }
                     this.changeFlightIdHandler=this.changeFlightIdHandler.bind(this);
                     this.searchFlightByFlightId=this.searchFlightByFlightId.bind(this);
    }
    
        
    changeFlightIdHandler=(event)=>{
        this.setState({flight_Id: event.target.value})
    }

    searchFlightByFlightId=(e)=>{
        e.preventDefault();
        let id =parseInt(this.state.flight_Id)
            this.props.history.push(`/displayflightdetails/`+id)
            
    }

    render() {
        return (
            <div>
                <br/>
                <br/>
            <div className="card col-md-3 offset-md-4 box-format">
                <br/><br/>
                <h2 className="text-center">Search Flight</h2>
              <form>           
                <table className="table table-center">
                  <tr> 
                    <td >  
                          <div className="form-group">
                             <input type="number" required="required" placeholder="Enter Flight Id " name="flight_Id" className="form-control"value={this.state.flight_Id} onChange={this.changeFlightIdHandler}/>
                         </div>      
                    </td>
                  </tr>
                <tr>
                   <td className="table-prop">
                        <input type="submit" value="Search" className="btn btn-warning" onClick={this.searchFlightByFlightId}/>  
                 </td>
                </tr>
              </table>
            </form>
        </div>
                        
        </div> 
        );
    }
}

export default SearchFlightById;