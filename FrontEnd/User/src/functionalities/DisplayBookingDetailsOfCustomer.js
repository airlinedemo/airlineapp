import React, { Component } from 'react';
import AllBookingDetailService from '../Services/AllBookingDetailService';

class DisplayBookingDetailsOfCustomer extends Component {
    constructor(props) {
        super(props);

        this.state = {
        bookings: []
    }
    this.bookingDetailsForTicket=this.bookingDetailsForTicket.bind(this);
      this.deleteBooking=this.deleteBooking.bind(this);
  }
   
  componentDidMount(){
      AllBookingDetailService.getAllBookingDetails(sessionStorage.getItem("loginEmail")).then((res)=>{
          this.setState({bookings:res.data});
      });
  }


  deleteBooking(id){
    this.props.history.push(`/deletebookingbyid/`+id)
  }
  bookingDetailsForTicket(id){
    this.props.history.push(`/downloadticket/`+id)  
  }
 
    render() {
        return (
            <div>
            <h2 className="text-center">Booking History</h2>
            <div className="row">
            <table className="table table-striped table-bordered" style={{margin: 20}}>
                      <thead>
                        <tr>
                          <th>Flight  No      </th>
                          <th>Flight  Name    </th>
                          <th>Flight  Class   </th>
                          <th>Journey Time    </th>
                          <th>Flight  Source  </th>
                          <th>Flight  Destination </th>
                          <th> Duration</th>
                          <th>Distance</th>
                          <th>No. of Passengers</th>
                          <th>status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      {       
                               this.state.bookings.map(b=>
                          
                                <tr  key={b.booking_Id}>
                                    <td> {b.flight.flight_No} </td>
                                    <td> {b.flight.flight_Name} </td>
                                    <td> {b.flight.flight_Class} </td>
                                    <td> {b.flight.journey_DateTime} </td>
                                    <td> {b.flight.route.source} </td>
                                    <td> {b.flight.route.destination} </td>
                                    <td> {b.flight.route.duration} </td>
                                    <td> {b.flight.route.distance} </td>
                                    <td> {b.no_Of_Passengers} </td>
                                    <td> {b.status?"Booked":"cancelled"} </td>
                                    <td>
                                    <input type="button" value="Ticket" className="btn btn-success" style={{marginLeft:10}} onClick={()=>this.bookingDetailsForTicket(b.booking_Id)}/>    
                                    <input type="button" value="Cancel" className="btn btn-danger" style={{marginLeft:10}} onClick={()=>this.deleteBooking(b.booking_Id)}/>

                                    </td>
                                </tr>
                                  
                                )
                          }
                      </tbody>
                  </table>
            </div>
        </div>
        );
    }
}

export default DisplayBookingDetailsOfCustomer;