import React, { Component } from 'react';
import SearchFlightByIdService from '../Services/SearchFlightByIdService';

class DisplaySearchedFlight extends Component {
    constructor(props){
    super(props);
        this.state = {
                       id :this.props.match.params.id,
                       data: 
                           {
                            flight_Id: '',
                            flight_No: '',
                            flight_Name: '',
                            sitting_Capacity: '',
                            flight_Class: '',
                            fair_per_Km: '',
                            journey_DateTime: '',
                            status: '',
                            route: {
                                route_Id: '',
                                source: '',
                                destination: '',
                                distance: '',
                                duration: '',
                                status: ''
                            }
                        }
                       
                     }
    }

    componentDidMount(){
        SearchFlightByIdService.searchFlightByFlightId(this.state.id).then((res)=>{
            this.setState({data:res.data});
            //console.log("state data");
            //console.log(this.state.data);
        });
    }
   
    render() {
        return (
            <div>
              <h2 className="text-center">Flight List</h2>
              <div className="row">
              <table className="table table-striped table-bordered" style={{margin: 20}}>
                        <thead>
                          <tr>
                            <th>Flight  No      </th>
                            <th>Flight  Name    </th>
                            <th>Flight  Capacity</th>
                            <th>Flight  Class   </th>
                            <th>Flight  Fare/km </th>
                            <th>Flight  Time    </th>
                            <th>Flight  Source  </th>
                            <th>Flight  Destination   </th>
                            <th>Flight Status</th>
                            
                          </tr>
                        </thead>
                        <tbody>
                               <tr key={this.state.data.flight_No}>
                                   <td>{this.state.data.flight_No}</td>
                                   <td>{this.state.data.flight_Name}</td>
                                   <td>{this.state.data.sitting_Capacity}</td>
                                   <td>{this.state.data.flight_Class}</td>
                                   <td>{this.state.data.fair_per_Km}</td>
                                   <td>{this.state.data.journey_DateTime}</td>
                                   <td>{this.state.data.route.source}</td>
                                   <td>{this.state.data.route.destination}</td>
                                   <td>{this.state.data.status?"Available":"Not Available"}</td>
                                   
                               </tr>
                        </tbody>
                    </table>
              </div>
              </div>
        );
    }
}

export default DisplaySearchedFlight;