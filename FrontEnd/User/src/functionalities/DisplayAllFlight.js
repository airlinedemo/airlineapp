import React, { Component } from 'react';
import DisplayAllFlightService from '../Services/DisplayAllFlightService';


class DisplayAllFlight extends Component {
    constructor(props) {
        super(props);

        this.state = {
        flights: []
    }
      
  }
   
  componentDidMount(){
      DisplayAllFlightService.getFlights().then((res)=>{
          this.setState({flights:res.data});
      });
  }


  bookFlightById(fid){
          let id =parseInt(fid)
            this.props.history.push(`/bookflightbyfid/`+id)
  }



  cancelFlightById(fId){
      
  }


  render() {


      return (
          <div>
              <h2 className="text-center">Flight List</h2>
              <div className="row">
              <table className="table table-striped table-bordered" style={{margin: 20}}>
                        <thead>
                          <tr>
                            <th>Flight  No      </th>
                            <th>Flight  Name    </th>
                            <th>Flight  Capacity</th>
                            <th>Flight  Class   </th>
                            <th>Flight  Fare/km </th>
                            <th>Flight  Time    </th>
                            <th>Flight  Source  </th>
                            <th>Flight  Destination   </th>
                            <th>Status</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                        {       
                                 this.state.flights.map(flight=>
                            
                                  <tr  key={flight.flight_Id}>
                                      <td> {flight.flight_No} </td>
                                      <td> {flight.flight_Name} </td>
                                      <td> {flight.sitting_Capacity} </td>
                                      <td> {flight.flight_Class} </td>
                                      <td> {flight.fair_per_Km} </td>
                                      <td> {flight.journey_DateTime} </td>
                                      <td> {flight.route.source} </td>
                                      <td> {flight.route.destination} </td>
                                      <td> {flight.status ? "Available" : "Not Available"} </td>
                                      <td>
                                      <input type="button" value="Book" className="btn btn-success" style={{marginLeft:10}} onClick={()=>this.bookFlightById(flight.flight_Id)}/>
                                      </td>
                                  </tr>
                                    
                                  )
                            }
                        </tbody>
                    </table>
              </div>
          </div>
      );
  }
}

export default DisplayAllFlight;