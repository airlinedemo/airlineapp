import React, { Component } from 'react';
import TicketGenerationService from '../Services/TicketGenerationService';


class DownloadTicket extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id :this.props.match.params.id,  
            booking: {
                booking_Id: "",
                flight: {
                    flight_Id: "",
                    flight_No: "",
                    flight_Name: "",
                    sitting_Capacity: "",
                    flight_Class: "",
                    fair_per_Km: "",
                    journey_DateTime: "",
                    status: "",
                    route: {
                        route_Id: "",
                        source: "",
                        destination: "",
                        distance: "",
                        duration: "",
                        status: ""
                    }
                },
                customer: {
                    id: "",
                    first_Name: "",
                    last_Name: "",
                    date_Of_Birth: "",
                    gender: "",
                    mobile_No: "",
                    email: "",
                    password: "",
                    city: "",
                    pincode: ""
                },
                booking_DateTime: "",
                no_Of_Passengers: "",
                fare: "",
                status: ""
            }
            
    }
    //this.generatepdf=this.generatepdf.bind(this)
    } 
    componentDidMount(){
       TicketGenerationService.getDetailsBYBookingId(this.state.id).then((res)=>{
            this.setState({booking:res.data});
        });
  
    }
   
   
 
    render() {
        return (
            <div>
                <div id="divToPrint">
                <table>
                <tr>
                    <td style={{width:800}}>
                    <div style={{marginRight:250}} >
                    <h4>Passenger Details</h4>
                    <table className="table table-striped table-bordered"   style={{margin: 20}}>

                      <tbody>

                                <tr>
                                    <td>Customer Name : {this.state.booking.customer.first_Name} {this.state.booking.customer.last_Name}</td>
                                    
                                 </tr>
                                <tr>                
                                    <td>Source: {this.state.booking.flight.route.source} </td>
                                    </tr>
                                <tr>  
                                    <td>Destination   :{this.state.booking.flight.route.destination} </td>     
                              
                               </tr>


                      </tbody>
                  </table>
                </div>
                    </td>
                    <td style={{width:500 }} >
                    <div>
                    <h4>Other Details</h4>
                    <table className="table table-striped table-bordered"   style={{margin: 20}}>

                      <tbody>

                                <tr>
                                    <td>Departure Time: {this.state.booking.flight.journey_DateTime} </td>
                                      </tr>
                                <tr>  
                                    <td>No. of Passengers: {this.state.booking.no_Of_Passengers} </td>
                                 </tr>
                                <tr>   
                                
                                   <td>Total Duration: {this.state.booking.flight.route.duration} </td> 
                                   </tr>
                                <tr>   
                                   <td>Payment Status:  {this.state.booking.status?"Paid":"Not Paid"}</td>
                               </tr>


                      </tbody>
                  </table>
                </div>
                    
                    </td>

                </tr>
                <tr>
                    <td style={{width:500}}>
                    <div style={{marginRight:250}}>
                <h4>Flight Details</h4>
                 <table className="table table-striped table-bordered"   style={{margin: 20}}>

                      <tbody>

                                <tr>
                                    <td>Flight  No.   : {this.state.booking.flight.flight_No} </td>
                                    </tr>
                                <tr>
                                    <td>Flight  Name  : {this.state.booking.flight.flight_Name} </td>
                                   
                                 </tr>
                                <tr>   
                                    
                                    <td>Flight  Class : {this.state.booking.flight.flight_Class} </td>
                                        
                               </tr>
                               <tr>
                              
                            </tr>
                      </tbody>
                  </table>
                </div>
                    </td>

                    </tr>
                   
                    </table>
                </div>

                        {/* <button className="btn btn-success btn-default" type="button" onClick={() => this.exportPDF()}>Download Ticket PDF</button> */}

            </div>
        );
    }
}

export default DownloadTicket;

