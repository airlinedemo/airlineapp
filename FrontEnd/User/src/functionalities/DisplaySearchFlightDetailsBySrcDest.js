import React, { Component } from 'react';
import SearchBySrcDestService from '../Services/SearchBySrcDestService';


class DisplaySearchFlightDetailsBySrcDest extends Component {

    constructor(props){
        super(props);
            this.state = {
                           src :    this.props.match.params.source,
                           dest:this.props.match.params.destination,
                           flights:[]
                        
                       
                         }
                
            this.gotoSignInPage=this.gotoSignInPage.bind(this)
        }


        gotoSignInPage(){
            alert("Sign In First");
            this.props.history.push("/signin")
        }
        
        componentDidMount(){
            SearchBySrcDestService.searchBySrcDest(this.state.src,this.state.dest).then((res)=>{
                this.setState({flights:res.data});
                console.log(res);
            });
        }

    render() {
        return (
            <div>
              <h2 className="text-center">Flight List</h2>
              <div className="row">
              <table className="table table-striped table-bordered" style={{margin: 20}}>
                        <thead>
                          <tr>
                            <th>Flight  No      </th>
                            <th>Flight  Name    </th>
                            <th>Flight  Capacity</th>
                            <th>Flight  Class   </th>
                            <th>Flight  Fare/km </th>
                            <th>Flight  Time    </th>
                            <th>Flight  Source  </th>
                            <th>Flight  Destination   </th>
                            <th>Flight Status</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                        
                            
                             <tr key={this.state.flights.flight_Id}>
                             <td>{this.state.flights.flight_No}</td>
                             <td>{this.state.flights.flight_Name}</td>
                             <td>{this.state.flights.sitting_Capacity}</td>
                             <td>{this.state.flights.flight_Class}</td>
                             <td>{this.state.flights.fair_per_Km}</td>
                             <td>{this.state.flights.journey_DateTime}</td>
                             <td>{this.state.src}</td>
                             <td>{this.state.dest}</td>
                             <td>{this.state.flights.status?"Available":"Not Available"}</td>
                             <td>
                                <input type="button" value="Book" className="btn btn-success" style={{marginLeft:10}} onClick={this.gotoSignInPage}/>
                             </td>
                         </tr>
                        </tbody>
                       </table>
                     </div>
                   </div>     
        );
    }
}

export default DisplaySearchFlightDetailsBySrcDest;