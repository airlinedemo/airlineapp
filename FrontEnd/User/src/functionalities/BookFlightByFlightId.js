import React, { Component } from 'react';
import BookingByFlightIdService from '../Services/BookingByFlightIdService';

class BookFlightByFlightId extends Component {
    constructor(props){
        super(props);
            this.state = {
                           fid :this.props.match.params.id ,
                           data:
                           {
                            
                            flight : {
                                       flight_Id : ''
                                     },
                                
                            customer : {
                                         id : ''
                                       },
                                
                            no_Of_Passengers : ''      
                            }       
                  } 

            this.changeCustIdHandler=this.changeCustIdHandler.bind(this); 
            this.changePassangerNoHandler=this.changePassangerNoHandler.bind(this);   
            this.bookFlightByCustId=this.bookFlightByCustId.bind(this);  
        }
     
        bookFlightByCustId=(e)=>{
        e.preventDefault();

        let newBooking = {
           flight:{
               flight_Id:this.state.fid
                  },
           customer : {
               id : sessionStorage.getItem("loginEmail")
                       },
            no_Of_Passengers:this.state.no_Of_Passengers
         
            }
        
      BookingByFlightIdService.bookFlightByCustId(newBooking).then(resp=>{
          //console.log(resp.data);
          this.setState({data:resp.data})
          alert("booked successfully");
          this.props.history.push("/displaybooking");
      })

        };

       
     
        changeCustIdHandler=e=>{
            this.setState({id:e.target.value})
           }
           changePassangerNoHandler=e=>{
           this.setState({no_Of_Passengers:e.target.value})
           }


    render() {
        return (
            <div className="container card col-md-4 offset-md-4">
                <h2 className="text-center">Booking</h2>
                <form >
                                      
                                 <div>
                                     <input className="form-control" type="number" required="required" placeholder="Enter no. of passanger" name="no_Of_Passengers"  value={this.state.no_Of_Passengers} onChange={this.changePassangerNoHandler}/>
                                 </div><br/>
                    
                    
                                 <div className="form-group">
                                  <input type="submit" value="Book" className="btn btn-warning" onClick={this.bookFlightByCustId}/>  
                                 </div>
                
                </form>
            </div>
        );
    }
}

export default BookFlightByFlightId;