import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router , Link, Route, Switch } from 'react-router-dom'

import HeaderComponent from './component/HeaderComponent';
import FooterComponent from './component/FooterComponent';
import CustomerPage from './component/CustomerPage';
import SignIn from './component/SignIn';
import RegisterCustomer from './component/RegisterCustomer';
import ForgetPassword from './component/ForgetPassword';
import About from './component/About';
import Contact from './component/Contact';
import HomePage from './component/HomePage';
import DisplayAllFlight from './functionalities/DisplayAllFlight';
import SearchFlightById from './functionalities/SearchFlightById';
import DisplaySearchedFlight from './functionalities/DisplaySearchedFlight';
import DisplaySearchFlightDetailsBySrcDest from './functionalities/DisplaySearchFlightDetailsBySrcDest';
import BookFlightByFlightId from './functionalities/BookFlightByFlightId';
import SearchBySourceAndDest from './functionalities/SearchBySourceAndDest';
import SignOut from './component/Signout';
import UpdateInfo from './component/UpdateInfo';
import DeleteAccount from './component/DeleteAccount';
import DisplayBookingDetailsOfCustomer from './functionalities/DisplayBookingDetailsOfCustomer';
import DisplayQuickBySourceAndDestination from './functionalities/DisplayQuickBySourceAndDestination';
import Profile from './component/Profile';
import DeleteBookedFlight from './functionalities/DeleteBookedFlight';
import DownloadTicket from './functionalities/DownloadTicket';


function App() {
  return (
    <div>
    <Router> 
    <HeaderComponent/>
        <nav className="navbar navbar-expand-lg navbar-dark " style={{backgroundColor:"#e3f2fd"}} >
              <ul className="navbar-nav mr-auto navbar-collapse">
              <li className=" nav-link"><Link className="link-color" to="/">Home</Link></li>
              <li className=" nav-link"><Link className="link-color" to="/about">About</Link></li>
              <li className=" nav-link"><Link className="link-color" to="/contact">Contact Us</Link></li>
              <li className=" nav-link"><Link className="link-color" to="/signin">SignIn</Link></li>
              
              </ul>
              <ul className="navbar-nav nav-item ">
              <li className=" me-md-2"><Link className="link-color" to="/signout">Log Out</Link></li>
              </ul>
       </nav> 
          
         <Switch>
           <div className="main-View">
             <Route path="/" exact component={HomePage}></Route>
             <Route path="/signin"  component={SignIn}></Route>
             <Route path="/customers" component={CustomerPage}></Route>
             <Route path="/customerregister" component={RegisterCustomer}></Route>
             <Route path="/forgetpassword" component={ForgetPassword}></Route>
             <Route path="/updatecustomerinfo"  component={UpdateInfo}></Route>
             <Route path="/deletecustomeraccount"  component={DeleteAccount}></Route>
             <Route path="/showcustomeraccount"  component={Profile}></Route>
             <Route path="/signout"  component={SignOut}></Route>

             <Route path="/displayallflights" component={DisplayAllFlight}></Route>
             <Route path="/searchflightbyid" component={SearchFlightById}></Route>
             <Route path="/deletebookingbyid/:id" component={DeleteBookedFlight}></Route>
             <Route path="/downloadticket/:id" component={DownloadTicket}></Route>
             <Route path="/searchflightbysrcdest" component={SearchBySourceAndDest}></Route>
             <Route path="/displaybooking" component={DisplayBookingDetailsOfCustomer}></Route>
             <Route path="/bookflightbyfid/:id"  component={BookFlightByFlightId}></Route>
             <Route path="/displayflightdetails/:id" component={DisplaySearchedFlight}></Route>
             <Route path="/displayflightdetailbysrcdest/:source/:destination" component={DisplaySearchFlightDetailsBySrcDest}></Route>
             <Route path="/displayquick/:source/:destination" component={DisplayQuickBySourceAndDestination}></Route>
             <Route path="/about" component={About}></Route>
             <Route path="/contact" component={Contact}></Route>

             </div>
        </Switch>
          <FooterComponent/>
    </Router>
   
  </div>
  );
}

export default App;
