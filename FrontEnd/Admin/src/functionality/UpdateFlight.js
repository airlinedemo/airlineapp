import React, { Component } from 'react';
import UpdateFlightService from '../Service/UpdateFlightService';

class UpdateFlight extends Component {

constructor(props){
super(props);
        
this.state = {
                       fid :this.props.match.params.id,
                       status:"",
                       route :{
                                route_Id : "",
                               
                            }
                      }
                 this.changeRouteIdHandler=this.changeRouteIdHandler.bind(this);
                 this.UpdateFlightById=this.UpdateFlightById.bind(this);
                 
                }
UpdateFlightById=(e)=>{
e.preventDefault();
    
let modifyFlight = {
         status:true,
         route : {
                  route_Id : this.state.route_Id
                  
                   }
};
            
UpdateFlightService.UpdateFlightById(this.state.fid,modifyFlight).then(res=>{
 // alert("Flight added succeddfully")     
    this.props.history.push('/displayallflights')
   });
}
  changeRouteIdHandler=(event)=>{
      this.setState({route_Id: event.target.value})
  }
    

    render() {
        return (
            <div>
                <div className="card col-md-4 offset-md-3" style={{marginTop: "20px"}}>
                 <br/>
                     <h2 className="text-color">Update Flight</h2>
                        <div >
                           <form>           
                    
                                <div>
                                     <input className="form-control" type="number" required="required" placeholder="Enter route_Id " name="route_Id" className="form-control" value={this.state.route_Id} onChange={this.changeRouteIdHandler}/>
                                </div><br/>  
                                <div>
                                 <input className="form-control" type="submit" value="Update Flight" className="btn btn-success" onClick={this.UpdateFlightById} />
                               </div><br/>  
                           </form>
                       </div>
                </div>
           </div>
        );
    }
}

export default UpdateFlight;