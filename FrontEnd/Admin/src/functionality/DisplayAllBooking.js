import React, { Component } from 'react';
import DisplayAllBookingService from './../Service/DisplayAllBookingService';


class DisplayAllBooking extends Component {
    constructor(props) {
        super(props);

        this.state = {
                       booking: []
                     }
               }

componentDidMount(){
            DisplayAllBookingService.displayBookingflights().then(res=>{
                console.log(res.data)
            this.setState({booking:res.data});
         
        });
    }

render() {
        return (
            <div>
                <h2 className="text-center">Booking List </h2>
                <div className="row">
                <table className="table table-striped table-bordered" style={{margin: 20}}>
                          <thead>
                            <tr>
                              <th>Booking Id</th>
                              <th>Flight No</th>
                              <th>Flight Name</th>
                              <th>Customer Id</th>
                              <th>Customer Name</th>
                              <th>Journey Time</th>
                              <th>no. of Passengers</th>
                              <th>Total Amount</th>
                              <th>Source</th>
                              <th>Destination</th>
                            </tr>
                          </thead>
                          <tbody>
                          {
                                  this.state.booking.map(b=>
                                    <tr key={b.booking_id}>
                                        
                                        <td> {b.booking_Id}</td>
                                        <td> {b.flight.flight_No} </td>
                                        <td> {b.flight.flight_Name} </td>
                                        <td> {b.customer.id} </td>
                                        <td> {b.customer.first_Name} </td>
                                        <td> {b.flight.journey_DateTime} </td>
                                        <td> {b.no_Of_Passengers} </td>
                                        <td> {b.fare} </td>
                                        <td> {b.flight.route.source} </td>
                                        <td> {b.flight.route.destination} </td>
                                    </tr>
                                    )
                              }
                          </tbody>
                      </table>
                </div>
            </div>
        );
    }
}

export default DisplayAllBooking;