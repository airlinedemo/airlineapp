import React, { Component } from 'react';
import FlightRegisterService from '../Service/FlightRegisterService';

class AddFlight extends Component {
  constructor(props){
        super(props);
        
        this.state = {
                       flight_No       :"",             
                       flight_Name     :"",
                       sitting_Capacity:"",
                       flight_Class    :"",
                       fair_per_Km     :"",
                       journey_DateTime:"",
                       status          :"",
                       route           :{
                                         route_Id : ""
                                     }
                 }
                 this.changeFNoHandler=this.changeFNoHandler.bind(this);
                 this.changeFlightNameHandler=this.changeFlightNameHandler.bind(this);
                 this.changeCapacityHandler=this.changeCapacityHandler.bind(this);
                 this.changeClassHandler=this.changeClassHandler.bind(this);
                 this.changeFareHandler=this.changeFareHandler.bind(this);
                 this.changeTimeHandler=this.changeTimeHandler.bind(this);
                 this.changeStatusHandler=this.changeStatusHandler.bind(this);
                 this.changeRouteIdHandler=this.changeRouteIdHandler.bind(this);
                 this.registerFlight=this.registerFlight.bind(this);
    
                }
        registerFlight=(e)=>{
            e.preventDefault();
    
            let newFlight = {
                       flight_No       : this.state.flight_No,             
                       flight_Name     : this.state.flight_Name,
                       sitting_Capacity: this.state.sitting_Capacity,
                       flight_Class    : this.state.flight_Class,
                       fair_per_Km     : this.state.fair_per_Km,
                       journey_DateTime: this.state.journey_DateTime,
                       status          : this.state.status,
                       route           : {
                                         route_Id : this.state.route_Id
                                      }
            };
            
            FlightRegisterService.registerFlight(newFlight).then(res=>{
            // alert("Flight added succeddfully")     
            this.props.history.push('/displayallflights')
            });
    
        }
        changeFNoHandler=(event)=>{
            this.setState({flight_No: event.target.value})
        }
    
        changeFlightNameHandler=(event)=>{
            this.setState({flight_Name: event.target.value})
        }
    
        changeCapacityHandler=(event)=>{
            this.setState({sitting_Capacity: event.target.value})
        }
    
        changeClassHandler=(event)=>{
            this.setState({flight_Class: event.target.value})
        }
    
    
        changeFareHandler=(event)=>{
            this.setState({fair_per_Km: event.target.value})
        }
    
    
        changeTimeHandler=(event)=>{
            this.setState({journey_DateTime: event.target.value})
        }
    
    
        changeStatusHandler=(event)=>{
            this.setState({status: event.target.value})
        }
    
    
        changeRouteIdHandler=(event)=>{
            this.setState({route_Id: event.target.value})
        }
    


    render() {
        return (
            <div>
                          <div className="card col-md-6 offset-md-3" style={{marginTop: "20px", backgroundColor: "#B2E541"}}>
                          <br/>
                          <h2 className="text-color">Add Flight</h2>

                         <div className="card-body">
                <form>           
                    <table>
                        <tr> 
                            <td className="table-prop">  
                                  <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter Flight No. " name="flight_No" className="form-control" value={this.state.flight_No} onChange={this.changeFNoHandler}/>
                                 </div><br/>      
                           </td>
                           <td  className="table-prop">
                                 <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter Flight Name " name="flight_Name" className="form-control" value={this.state.flight_Name} onChange={this.changeFlightNameHandler}/>
                                 </div><br/>
                            </td>
                        </tr>
                        <tr> 
                            <td className="table-prop">

                                 <div className="form-group">
                                     <input type="number" required="required" placeholder="Enter Flight Capacity" name="sitting_Capacity" className="form-control" value={this.state.sitting_Capacity} onChange={this.changeCapacityHandler}/>
                                 </div><br/>
                           </td>
                           <td className="table-prop">
                           <div className="form-group">
                                     {/* <input type="text" required="required" placeholder="Enter Flight class" name="flight_Class" className="form-control" value={this.state.flight_Class} onChange={this.changeClassHandler}/> */}
                                     <select className="table-prop"  name="gender" required="required"  onChange={this.changeClassHandler}  >
                                       <option value="All">All</option>
                                       <option value="Economy">Economy</option>
                                       <option value="Business">Business</option>
                                       <option selected>Select Class</option>
                                     </select>
                                 </div><br/> 
                             </td>
                        </tr>
                        <tr> 
                            <td className="table-prop">
                            <div className="form-group">
                                     <input type="number" required="required" placeholder="Enter Fare/km" name="fair_per_Km" className="form-control" value={this.state.fair_per_Km} onChange={this.changeFareHandler}/>
                                 </div><br/> 
                            </td>
                            <td className="table-prop">
                            <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter Date-Time: YYYY-MM-DD HH:MM:SS " name="journey_DateTime" className="form-control" value={this.state.journey_DateTime} onChange={this.changeTimeHandler}/>
                                 </div><br/> 
                            </td>
                        </tr>
                        <tr> 
                            <td className="table-prop">
                            <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter Status" name="status" className="form-control" value={this.state.status} onChange={this.changeStatusHandler}/>
                                 </div><br/>  
                           </td>
                           <td className="table-prop">
                            <div className="form-group">
                                     <input type="number" required="required" placeholder="Enter route_Id " name="route_Id" className="form-control" value={this.state.route_Id} onChange={this.changeRouteIdHandler}/>
                                 </div><br/>  
                           </td>
                           </tr>
           
                          <tr aria-colspan="2">
                               <div className="button-position">
                                 <input type="submit" value="Save Flight" className="btn btn-success" onClick={this.registerFlight}/>
                               </div>
                         </tr>
                    </table>
              </form>
             </div>
             </div>
           </div>
            
        );
    }
}

export default AddFlight;