import React, { Component } from 'react';
import DisplayAllAdminService from '../Service/DisplayAllAdminService';

class DisplayAllAdmin extends Component {
    constructor(props) {
        super(props);

        this.state = {
        admins: []
    }
  }
   
  componentDidMount(){
      DisplayAllAdminService.getAdmin().then((res)=>{
          this.setState({admins:res.data});
      });
  }

    render() {
        return (
            <div>
            <h2 className="text-center">Admin List</h2>
            <div className="row">
            <table className="table table-striped table-bordered" style={{margin: 20}}>
                      <thead>
                        <tr>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Birth Date</th>
                          <th>Mobile No</th>
                          <th>Email Id</th>
                          <th>Password</th>
                          <th>City Name</th>
                          <th>Pincode</th>
                        </tr>
                      </thead>
                      <tbody>
                      {
                              this.state.admins.map(admin=>
                                 <tr key={admin.id}>
                                    <td> {admin.first_Name} </td>
                                    <td> {admin.last_Name} </td>
                                    <td> {admin.date_Of_Birth} </td>
                                    <td> {admin.mobile_No} </td>
                                    <td> {admin.email} </td>
                                    <td> {admin.password} </td>
                                    <td> {admin.city} </td>
                                    <td> {admin.pincode} </td>
                                </tr>
                                )
                          }
                      </tbody>
                  </table>
            </div>
        </div>
        );
    }
}

export default DisplayAllAdmin ;