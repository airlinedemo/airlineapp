import React, { Component } from 'react';
import AddRouteService from '../Service/AddRouteService';

class AddRoute extends Component {
    constructor(props){
        super(props);
        
        this.state = {
                   source   : "",
                   destination: "",
                   distance : "",
                   duration : "",
                   status   :" "
                }
                 this.changeSourceHandler=this.changeSourceHandler.bind(this);
                 this.changeDestinationHandler=this.changeDestinationHandler.bind(this);
                 this.changeDistanceHandler=this.changeDistanceHandler.bind(this);
                 this.changeDurationHandler=this.changeDurationHandler.bind(this);
                 this.changeStatusHandler=this.changeStatusHandler.bind(this);
                 this.addRoute=this.addRoute.bind(this);
    
                }
                addRoute=(e)=>{
                 e.preventDefault();
    
              let newRoute = {
                           source     : this.state.source,
                           destination: this.state.destination,
                           distance   : this.state.distance,
                           duration   : this.state. duration ,
                           status     : this.state.status
                  };
            
            AddRouteService.addRoute(newRoute).then(res=>{
            // alert("Flight added succeddfully")     
            this.props.history.push('/displayallroute')
            });
    
        }
        changeSourceHandler=(event)=>{
            this.setState({source: event.target.value})
        }
    
        changeDestinationHandler=(event)=>{
            this.setState({destination: event.target.value})
        }
    
        changeDistanceHandler=(event)=>{
            this.setState({distance: event.target.value})
        }
    
        changeDurationHandler=(event)=>{
            this.setState({duration: event.target.value})
        }
        changeStatusHandler=(event)=>{
            this.setState({status: event.target.value})
        }
    
    


    render() {
        return (
            <div>
                          <div className="card col-md-4 offset-md-4" style={{marginTop: "20px", backgroundColor: "#B2E541"}}>
                          <br/>
                          <h2 className="text-color">Add Route</h2>

                         <div className="card-body">
                <form>           
                    <table >
                        <tr> 
                             
                                  <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter source " name="source" className="form-control" value={this.state.source} onChange={this.changeSourceHandler}/>
                                 </div><br/>      
                           
                        </tr>
                        <tr>  
                                 <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter destination " name="destination" className="form-control" value={this.state.destination} onChange={this.changeDestinationHandler}/>
                                 </div><br/>
                           
                        </tr>
                        <tr> 
                          

                                 <div className="form-group">
                                     <input type="number" required="required" placeholder="Enter distance" name="distance" className="form-control" value={this.state.distance} onChange={this.changeDistanceHandler}/>
                                 </div><br/>
                                 </tr>
                        <tr>  
                           
                            
                            <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter duration " name="duration" className="form-control" value={this.state.duration} onChange={this.changeDurationHandler}/>
                                 </div><br/> 
                           
                        </tr>
                        <tr>
                        
                            <div className="form-group">
                                     {/* <input type="text" required="required" placeholder="Enter Status " name="status" className="form-control" value={this.state.status} onChange={this.changeStatusHandler}/> */}
                                     <select className="table-prop"  name="status" required="required"  onChange={this.changeStatusHandler}  >
                                       <option value="true">true</option>
                                       <option value="false">false</option>
                                       <option selected>Select Status</option>
                                     </select>
                                 </div><br/> 
                            
                            </tr>
                          <tr aria-colspan="2">
                               <div className="button-position">
                                 <input type="submit" value="Save Route" className="btn btn-success" onClick={this.addRoute}/>
                               </div>
                         </tr>
                    </table>
              </form>
             </div>
             </div>
           </div>
            
        );
    }
}

export default AddRoute;