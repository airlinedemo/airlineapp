import React, { Component } from 'react';
import DeleteRouteService from '../Service/DeleteRouteService';
import RouteDisplayService from '../Service/RouteDisplayService';

class DisplayAllRoutes extends Component {

  constructor(props) {
      super(props);
     
      this.state={
        route:[]
      }  
  }
  
componentDidMount(){
    RouteDisplayService.displayRoutes().then(res=>{
        this.setState({route:res.data});
    });
}

modifyRoute(id){
  this.props.history.push("updateroute/"+id)
}

deleteRoute(rId){
  DeleteRouteService.deleteRouteById(rId).then( res=>{
   this.setState({route:this.state.route.filter
    (route=>route.route_Id!=rId)
   });
  })
}

    render() {
        return (
            <div>
               <br/> <br/>
              <h2 className="text-center">All Available Routes</h2>
              <div className="row">
              <table className="table table-striped table-bordered" style={{margin: 50}}>
                        <thead>
                          <tr>
                            <th>Route Source    </th>
                            <th>Route Destination</th>
                            <th>Route Distance   </th>
                            <th>Duration </th>
                            <th>Status </th>
                            <th>Actions</th>
                           </tr>
                        </thead>
                        <tbody>
                        {
                                 this.state.route.map((route)=>
                                  <tr  key={route.route_Id}>
                                      <td> {route.source} </td>
                                      <td> {route.destination} </td>
                                      <td> {route.distance} </td>
                                      <td> {route.duration} </td>
                                      <td> {route.status?"Available":"Disable" } </td>
                                      <td> 
                                      <input type="button" value="Update" className="btn btn-success" style={{marginLeft:10}} onClick={()=>this.modifyRoute(route.route_Id)}/>
                                      <input type="button" value="Delete" className="btn btn-danger" style={{marginLeft:10}} onClick={()=>this.deleteRoute(route.route_Id)}/>
                                      </td>
                                  </tr>
                                  )
                            }
                        </tbody>
                    </table>
              </div>
          
            </div>
        );
    }
}

export default DisplayAllRoutes;