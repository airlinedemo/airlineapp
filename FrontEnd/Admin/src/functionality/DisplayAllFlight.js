import React, { Component } from 'react';
import DisplayAllFlightService from '../Service/DisplayAllFlightService';
import DeleteFlightService from './../Service/DeleteFlightService';


class DisplayAllFlight extends Component {
    constructor(props) {
        super(props);

        this.state = {
        flights: []
    }
      
  }
   
  componentDidMount(){
      DisplayAllFlightService.getFlights().then((res)=>{
          this.setState({flights:res.data});
      });
  }


  modifyforword(fid){
    this.props.history.push('/updateflight/'+fid)
  }



  deleteFlight(fId){
      DeleteFlightService.deleteFlightById(fId).then( res=>{
       this.setState({flights:this.state.flights.filter
        (flight=>flight.flight_Id!=fId)
       });
      })
  }


  render() {


      return (
          <div>
              <h2 className="text-center">Flight List</h2>
              <div className="row">
              <table className="table table-striped table-bordered" style={{margin: 20}}>
                        <thead>
                          <tr>
                            <th>Flight  No      </th>
                            <th>Flight  Name    </th>
                            <th>Flight  Capacity</th>
                            <th>Flight  Class   </th>
                            <th>Flight  Fare/km </th>
                            <th>Flight  Time    </th>
                            <th>Flight  Source  </th>
                            <th>Flight  Destination   </th>
                            <th>Status</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                        {       
                                 this.state.flights.map(flight=>
                            
                                  <tr  key={flight.flight_Id}>
                                      <td> {flight.flight_No} </td>
                                      <td> {flight.flight_Name} </td>
                                      <td> {flight.sitting_Capacity} </td>
                                      <td> {flight.flight_Class} </td>
                                      <td> {flight.fair_per_Km} </td>
                                      <td> {flight.journey_DateTime} </td>
                                      <td> {flight.route.source} </td>
                                      <td> {flight.route.destination} </td>
                                      <td> {flight.status ? "Active" : "Disable"} </td>
                                      <td>
                                      <input type="button" value="Update" className="btn btn-success" style={{marginLeft:10}} onClick={()=>this.modifyforword(flight.flight_Id)}/>
                                      <input type="button" value="Delete" className="btn btn-danger" style={{marginLeft:10}} onClick={()=>this.deleteFlight(flight.flight_Id)}/>
                                      </td>
                                  </tr>
                                    
                                  )
                             }  
                        </tbody>
                    </table>
              </div>
          </div>
      );
  }
}

export default DisplayAllFlight;