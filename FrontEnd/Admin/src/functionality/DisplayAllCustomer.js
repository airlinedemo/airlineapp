import React, { Component } from 'react';
import DisplayAllCustomerService from '../Service/DisplayAllCustomerService';


class DisplayAllCustomer extends Component {
       
    constructor(props) {
          super(props);

          this.state = {
          customers: []
      }

    }
     
    componentDidMount(){
        DisplayAllCustomerService.getCustomers().then((res)=>{
            this.setState({customers:res.data});
        });
    }

    render() {


        return (
            <div>
                <h2 className="text-center">Customer List</h2>
                <div className="row">
                <table className="table table-striped table-bordered" style={{margin: 20}}>
                          <thead>
                            <tr>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Birth Date</th>
                              <th>Gender</th>
                              <th>Mobile No</th>
                              <th>Email Id</th>
                              <th>Password</th>
                              <th>City Name</th>
                              <th>Pincode</th>
                            </tr>
                          </thead>
                          <tbody>
                          {
                                  this.state.customers.map(customer=>
                                    <tr key={customer.id}>
                                        <td> {customer.first_Name} </td>
                                        <td> {customer.last_Name} </td>
                                        <td> {customer.date_Of_Birth} </td>
                                        <td> {customer.gender} </td>
                                        <td> {customer.mobile_No} </td>
                                        <td> {customer.email} </td>
                                        <td> {customer.password} </td>
                                        <td> {customer.city} </td>
                                        <td> {customer.pincode} </td>
                                    </tr>
                                    )
                              }
                          </tbody>
                      </table>
                </div>
            </div>
        );
    }
}

export default DisplayAllCustomer;