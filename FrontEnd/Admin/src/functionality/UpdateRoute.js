import React, { Component } from 'react';
import UpdateRouteService from './../Service/UpdateRouteService';

class UpdateRoute extends Component {

    
    constructor(props){
        super(props);
        
    this.state = {
                          id:this.props.match.params.id,                       
                          source: "",
                          destination: "",
                          distance: "",
                          duration: "",
                          status: false
                      
  }

  this.changeSourceIdHandler=this.changeSourceIdHandler.bind(this);
  this.changeDestinationIdHandler=this.changeDestinationIdHandler.bind(this);
  this.changeDistanceIdHandler=this.changeDistanceIdHandler.bind(this);
  this.changeDurationIdHandler=this.changeDurationIdHandler.bind(this);
  this.updateRouteMethod=this.updateRouteMethod.bind(this);
 }


 updateRouteMethod=(e)=>{
     e.preventDefault();

     let modifiedRoute = {
                             source: this.state.source,
                             destination: this.state.destination,
                             distance:  this.state.distance,
                             duration: this.state.duration,
                             status:  true
                       
                         };

UpdateRouteService.updateRoute(this.state.id,modifiedRoute).then(res=>{    
this.props.history.push('/displayallroute')
});

}

changeSourceIdHandler=(event)=>{
this.setState({source: event.target.value})
}

changeDestinationIdHandler=(event)=>{
this.setState({destination: event.target.value})
}

changeDistanceIdHandler=(event)=>{
this.setState({distance: event.target.value})
}

changeDurationIdHandler=(event)=>{
this.setState({duration: event.target.value})
}

    render() {
        return (
            <div>
            <div className="card col-md-3 offset-md-4" style={{marginTop: "20px", backgroundColor: "#B2E541"}}>
            <br/>
            <h2 className="text-color">Update Route</h2>

           <div className="card-body">
  <form>           

              <div className="form-group">
                       <input type="text" required="required" placeholder="Enter source " name="source" className="form-control" value={this.state.source} onChange={this.changeSourceIdHandler}/>
                   </div>  

              <div className="form-group">
                       <input type="text" required="required" placeholder="Enter Destination " name="destination" className="form-control" value={this.state.destination} onChange={this.changeDestinationIdHandler}/>
                   </div>

              <div className="form-group">
                       <input type="number" required="required" placeholder="Enter Distance " name="distance" className="form-control" value={this.state.distance} onChange={this.changeDistanceIdHandler}/>
                   </div>  

              <div className="form-group">
                       <input type="number" required="required" placeholder="Enter Duration " name="duration" className="form-control" value={this.state.duration} onChange={this.changeDurationIdHandler}/>
                   </div>  

                 <div className="button-position">
                   <input type="submit" value="Update Route" className="btn btn-success" onClick={this.updateRouteMethod} />
                 </div>

</form>
</div>
</div>
</div>
        );
    }
}

export default UpdateRoute;