import React, { Component } from 'react';

class Contact extends Component {
    render() {
        return (
            <div className="contact-group text-center">
                
                <h2><u> Contact Us</u></h2>
                <br />
                <img src='https://sustainablesquare.com/wp-content/uploads/2017/05/map-location-pin.jpg' width="100%" height="330px" alt="location" class="img-responsive " />
                <br />
                <div className="row">
                <div className="container">
                
                <table className="table table-prop">
                    <tr>
                        <td>Mobile : </td>
                        <td> 9972201498 </td>
                    </tr>
                    <tr>
                        <td>Whatsapp : </td>
                        <td> 9972201498 </td>
                    </tr>
                    </table>
                </div>
                <div>
                    <table className="table table-prop ">
                    <tr>
                        <td>Email : </td>
                        <td><strong> prem.sandesh.chavan98@gmail.com</strong> </td>
                    </tr>
                    <tr>
                        <td>Address: </td>
                        <td>Sr.No 222, Opp Chhatrapati Shivaji Sports, MITCON Lane, Pune 40001 </td>
                    </tr>
                </table> 
                <br/> 
                 </div>             
               </div>
            </div>
        );
    }
}

export default Contact;