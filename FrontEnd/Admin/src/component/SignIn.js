import React, { Component } from 'react';
import AdminLoginService from '../Service/AdminLoginService';



class SignIn extends Component {

    constructor(props) {
        super(props);
        
        this.state={
            email:"",
            password:""
        }
        this.changeEmailHandler = this.changeEmailHandler.bind(this);
        this.changePasswordHandler = this.changePasswordHandler.bind(this)
        this.loginAdmin = this.loginAdmin.bind(this);
    }
    
    loginAdmin=(e)=>{
        e.preventDefault();

        let login = {
            email:this.state.email,
            password:this.state.password
        };

       // console.log("login=>"+JSON.stringify(login));
       AdminLoginService.loginAdmin(login).then(res=>{  
        sessionStorage.setItem("loginId",res.data.id)
        sessionStorage.setItem("AdminName",res.data.first_Name)
        //  alert("Signin Successfully")
        this.props.history.push('/admin') 
     });
    }

    changeEmailHandler=(event)=>{
        this.setState({email: event.target.value});
    }

    changePasswordHandler=(event)=>{
        this.setState({password: event.target.value});
    }
    
    RegisterAdmin=(e)=>{
        this.props.history.push("/adminregister")
    }

    render() {
        return (
            <div >
               <div className="container-md bg-img ">
                   <div className="row">
                      <div className="card col-md-3 offset-md-1 , form-formatt" style={{marginTop: "20px"}}>
                          <br/>
                         <h2 className="text-colour">Admin Login</h2>
                        
                         <div className="card-body">
                             <form>
                                 <div className="form-group">
                                     <input type="email" required="required" placeholder="Enter Email" name="email" className="form-control"
                                     value={this.state.email} onChange={this.changeEmailHandler}/>
                                 </div><br/>

                                 <div className="form-group">
                                     <input type="password" required="required" placeholder="Enter Password" name="password" className="form-control"
                                     value={this.state.password} onChange={this.changePasswordHandler}/>
                                 </div>
                                 
                                 <div >
                                    {/* <a className="password-color" href="/forgetpassword">Forgot Password ?</a> */}
                                </div><br/>
                                 <table>
                                 <td width="50%">    
                                 <input type="submit" value="Sign In" className="btn btn-success" onClick={this.loginAdmin}/>
                                 </td>
                                <td> 
                                 <input type="button" value="Register" className="btn btn-warning" onClick={this.RegisterAdmin}/>
                                 </td> 
                                 </table>
                             </form>
                         </div> 
                      </div>       
                   </div>
               </div>
            </div>
        );
    }
}

export default SignIn;