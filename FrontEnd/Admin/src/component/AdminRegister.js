import React, { Component } from 'react';
import AdminRegisterService from '../Service/AdminRegisterService';

class AdminRegister extends Component {


    constructor(props){
        super(props);
        
        this.state = {
                           first_Name:"",
                           last_Name:"",
                           date_Of_Birth:"",
                           mobile_No:"",
                           email:"",
                           password:"",
                           city:"",
                           pincode:""
                 }
                 this.changeFnameHandler=this.changeFnameHandler.bind(this);
                 this.changeLnameHandler=this.changeLnameHandler.bind(this);
                 this.changeDOBHandler=this.changeDOBHandler.bind(this);
                 this.changeMobileHandler=this.changeMobileHandler.bind(this);
                 this.changeEmailHandler=this.changeEmailHandler.bind(this);
                 this.changePasswordHandler=this.changePasswordHandler.bind(this);
                 this.changeCityHandler=this.changeCityHandler.bind(this);
                 this.changePincodeHandler=this.changePincodeHandler.bind(this);
                 this.registerAdmin=this.registerAdmin.bind(this);
    
                }
        registerAdmin=(e)=>{
            e.preventDefault();
    
            let newAdmin = {
                first_Name:this.state.first_Name,
                last_Name:this.state.last_Name,
                date_Of_Birth:this.state.date_Of_Birth,
                mobile_No:this.state.mobile_No,
                email:this.state.email,
                password:this.state.password,
                city:this.state.city,
                pincode:this.state.pincode
            };
            
            AdminRegisterService.registerAdmin(newAdmin).then(res=>{
            // alert("Registered Successfully")     
            this.props.history.push('/signin')
            });
    
        }
        changeFnameHandler=(event)=>{
            this.setState({first_Name: event.target.value})
        }
    
        changeLnameHandler=(event)=>{
            this.setState({last_Name: event.target.value})
        }
    
        changeDOBHandler=(event)=>{
            this.setState({date_Of_Birth: event.target.value})
        }
    
        changeMobileHandler=(event)=>{
            this.setState({mobile_No: event.target.value})
        }
    
    
        changeEmailHandler=(event)=>{
            this.setState({email: event.target.value})
        }
    
    
        changePasswordHandler=(event)=>{
            this.setState({password: event.target.value})
        }
    
    
        changeCityHandler=(event)=>{
            this.setState({city: event.target.value})
        }
    
    
        changePincodeHandler=(event)=>{
            this.setState({pincode: event.target.value})
        }
    


    render() {
        return (
            <div>
                            <div>
                          <div className="card col-md-6 offset-md-3" style={{marginTop: "20px", backgroundColor: "#B2E541"}}>
                          <br/>
                          <h2 className="text-color">Register</h2>

                         <div className="card-body">
                <form>           
                    <table>
                        <tr> 
                            <td className="table-prop">  
                                  <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter First Name " name="first_Name" className="form-control" value={this.state.first_Name} onChange={this.changeFnameHandler}/>
                                 </div><br/>      
                           </td>
                           <td  className="table-prop">
                                 <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter Last Name " name="last_Name" className="form-control" value={this.state.last_Name} onChange={this.changeLnameHandler}/>
                                 </div><br/>
                            </td>
                        </tr>
                        <tr> 
                            <td className="table-prop">

                                 <div className="form-group">
                                     <input type="date" required="required" placeholder="Enter Date of Birth" name="date_Of_Birth" className="form-control" value={this.state.date_Of_Birth} onChange={this.changeDOBHandler}/>
                                 </div><br/>
                           </td>
                           <td className="table-prop">
                           <div className="form-group">
                                     <input type="number" required="required" placeholder="Enter Mobile Number" name="mobile" className="form-control" value={this.state.mobile} onChange={this.changeMobileHandler}/>
                                 </div><br/> 
                             </td>
                        </tr>
                        <tr> 
                            <td className="table-prop">
                            <div className="form-group">
                                     <input type="email" required="required" placeholder="Enter Email" name="email" className="form-control" value={this.state.email} onChange={this.changeEmailHandler}/>
                                 </div><br/> 
                            </td>
                            <td className="table-prop">
                            <div className="form-group">
                                     <input type="password" required="required" placeholder="Enter Password" name="password" className="form-control" value={this.state.password} onChange={this.changePasswordHandler}/>
                                 </div><br/> 
                            </td>
                        </tr>
                        <tr> 
                            <td className="table-prop">
                            <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter City Name" name="city" className="form-control" value={this.state.city} onChange={this.changeCityHandler}/>
                                 </div><br/>  
                           </td>
                           <td className="table-prop">
                           <div className="form-group">
                                     <input type="text" required="required" placeholder="Enter Pincode" name="pincode" className="form-control" value={this.state.pincode} onChange={this.changePincodeHandler} />
                                 </div><br/> 
                            </td>
                        </tr>   
                          <tr aria-colspan="2">
                               <div className="button-position">
                                 <input type="submit" value="Register" className="btn btn-success" onClick={this.registerAdmin}/>
                               </div>
                         </tr>
                    </table>
              </form>
             </div>
             </div>
           </div>
            </div>
        );
    }
}

export default AdminRegister;