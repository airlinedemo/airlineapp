import React, { Component } from 'react';

class HomePage extends Component {
    render() {
        return (
            <div className="home-page">
                <br/>
                <div className="welcome-text" >
                <h1>Welcome to Airline Reservation System</h1>
                </div>
            </div>
        );
    }
}

export default HomePage;