import React, { Component} from 'react';

class AdminPage extends Component {

    render() {
        return (
          <div className="home-page">
            <div className="admin-op">
                <br />
                <h4 >Hello {sessionStorage.getItem("AdminName")}</h4>
                <h2 className="txt-shadow" style={{color:'white' ,textShadow:2}}>Admin Operations</h2>
                <br/>
                <div>
                <table >
                <tr>
                    <td >
                    <div class="dropdown" style={{marginLeft:300}} >
                       <button class="dropbtn" style={{marginLeft:100}}>Display functionalities</button>
                       <div class="dropdown-content">
                         <a className="btn btn-primary" href="/displayalladmin"> All Admin</a>
                         <a className="btn btn-secondary" href="/displayallcustomer"> All Customers</a>
                         <a className="btn  btn-info" href="/displayallroute"> All Routes</a>
                         <a className="btn btn-dark" href="/displayallflights"> All Flights</a>
                         <a className="btn btn-warning" href="/displayallbooking"> All Booking</a>
                       </div>
                      </div>
                    </td>
                    <td >
                    <div class="dropdown" style={{marginLeft:250}}>
                       <button class="dropbtn" style={{marginLeft:100}}>Add functionalities</button>
                       <div class="dropdown-content">
                         <a className="btn btn-secondary" href="/addflight"> Save Flights</a>
                         <a className="btn btn-primary" href="/addroute"> Save Routes</a>
                       </div>
                      </div>
                    </td>
                   
                </tr>    
               
                </table>
                </div>
                </div>
            </div>   
        );
    }
}

export default AdminPage;