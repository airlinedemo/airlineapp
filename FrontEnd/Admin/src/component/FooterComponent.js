import React, { Component } from 'react';

class FooterComponent extends Component {
    render() {
        return (
            <div>
                <footer className="footer">
                    <span style={{verticalAlign:"middle"}}>CopyRight @2021 AirlineReservationSystem</span>
                </footer>
            </div>
        );
    }
}

export default FooterComponent;