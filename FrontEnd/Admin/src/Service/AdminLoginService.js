import axios from 'axios';

const URL ="http://localhost:8080/user-api/loginreg/loginAdmin"

class AdminLoginService{

    loginAdmin(login){
        return axios.post(URL, login);
    }

}
export default new AdminLoginService();
