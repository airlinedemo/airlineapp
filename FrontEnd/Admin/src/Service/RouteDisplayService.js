import axios from 'axios'

const URL="http://localhost:8080/user-api/route/"

class RouteDisplayService  {
   
displayRoutes(){
    return axios.get(URL);
}

}

export default new RouteDisplayService();

