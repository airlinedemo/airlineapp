import axios from 'axios'

const URL="http://localhost:8080/user-api/flight/updateFlight/"
class UpdateFlightService {
 
 UpdateFlightById(Id ,modifyFlight){
     return axios.put(URL+Id,modifyFlight)
 }   
}

export default new UpdateFlightService();