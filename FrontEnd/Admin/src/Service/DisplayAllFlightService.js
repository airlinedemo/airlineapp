import axios from 'axios' 

const URL ="http://localhost:8080/user-api/flight/getAllFlights"

class DisplayAllFlightService {
    
    getFlights(){
        return axios.get(URL);
    }   
}

export default new DisplayAllFlightService()