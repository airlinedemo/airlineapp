import axios from 'axios';

const URL="http://localhost:8080/user-api/route/saveRoute/"
class AddRouteService{
   
addRoute(newRoute){
   return axios.post(URL , newRoute);
  }  
}

export default new AddRouteService();