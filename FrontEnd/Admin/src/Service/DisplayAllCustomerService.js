import axios from 'axios' 

const URL ="http://localhost:8080/user-api/loginreg/getAllCustomer"

class DisplayAllCustomerService {
    
    getCustomers(){
        return axios.get(URL);
    }   
}

export default new DisplayAllCustomerService()