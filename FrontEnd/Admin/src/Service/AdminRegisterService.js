import axios from 'axios'

 const URL = "http://localhost:8080/user-api/loginreg/admin"
class AdminRegisterService {

    registerAdmin=(newAdmin)=>{
        return axios.post(URL, newAdmin);
    }

}

export default new AdminRegisterService();