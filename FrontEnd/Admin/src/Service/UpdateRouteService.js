import axios from 'axios'

const URL="http://localhost:8080/user-api/route/update/"
class UpdateRouteService  {

    updateRoute(id,modifyObj){
        return axios.put(URL+id,modifyObj);
    }

}

export default new UpdateRouteService();