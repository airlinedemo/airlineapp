import axios from 'axios'

const URL="http://localhost:8080/user-api/flight/deleteFlightById/"
class DeleteFlightService{
   
deleteFlightById(fid){
  return axios.put(URL + fid)
}

}

export default new DeleteFlightService();