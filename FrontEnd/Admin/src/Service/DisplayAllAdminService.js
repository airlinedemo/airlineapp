import axios from 'axios' 

const URL ="http://localhost:8080/user-api/loginreg/admin/"

class DisplayAllAdminService {
    
    getAdmin(){
        return axios.get(URL);
    }   
}

export default new DisplayAllAdminService()