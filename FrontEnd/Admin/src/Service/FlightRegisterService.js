import axios from 'axios'

const URL = "http://localhost:8080/user-api/flight/flights/"

class FlightRegisterService  {
       
        registerFlight=(newFlight)=>{
            return axios.post(URL, newFlight);
        }
}

export default new FlightRegisterService();