import axios from 'axios'

const URL="http://localhost:8080/user-api/route/deleteRoute/0/"
class DeleteRouteService{
   
deleteRouteById(rid){
  return axios.put(URL + rid)
}

}

export default new DeleteRouteService();