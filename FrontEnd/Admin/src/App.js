
import './App.css';
import { BrowserRouter as Router,Link, Route, Switch } from 'react-router-dom'
import SignIn from './component/SignIn';
import AdminPage from './component/AdminPage';
import HeaderComponent from './component/HeaderComponent';
import FooterComponent from './component/FooterComponent';
import AdminRegister from './component/AdminRegister';
import HomePage from './component/HomePage';
import About from './component/About';
import Contact from './component/Contact';
import AddFlight from './functionality/AddFlight';
import UpdateFlight from './functionality/UpdateFlight';
import DisplayAllCustomer from './functionality/DisplayAllCustomer';
import DisplayAllFlight from './functionality/DisplayAllFlight';
import DisplayAllRoutes from './functionality/DisplayAllRoutes';
import AddRoute from './functionality/AddRoute';
import DisplayAllAdmin from './functionality/DisplayAllAdmin';
import DisplayAllBooking from './functionality/DisplayAllBooking';
import UpdateRoute from './functionality/UpdateRoute';
import SignOut from './component/SignOut';




function App() {
 
  return (
   
    <div>
     <Router>
     <HeaderComponent />
     <nav class="navbar navbar-expand-lg navbar-dark " style={{backgroundColor:"#e3f2fd"}} >
       <ul className="navbar-nav mr-auto navbar-collapse">
       <li className=" nav-link"><Link className="link-color" to="/">Home</Link></li>
       <li className=" nav-link"><Link className="link-color" to="/about">About</Link></li>
       <li className=" nav-link"><Link className="link-color" to="/contact">Contact Us</Link></li>
       <li className=" nav-link"><Link className="link-color" to="/signin">SignIn</Link></li>
       </ul>
    
              <ul className="navbar-nav nav-item ">
              <li className=" me-md-2"><Link className="link-color" to="/signout">Log Out</Link></li>
              </ul>
       </nav> 
      <Switch>
             <Route path="/" exact component={HomePage}></Route>
             <Route path="/signin" exact component={SignIn}></Route>
             <Route path="/admin" component={AdminPage}></Route>
             <Route path="/adminregister" component={AdminRegister}></Route>
             <Route path="/signout"  component={SignOut}></Route>
             <Route path="/displayallflights" component={DisplayAllFlight}></Route>
             <Route path="/addflight" component={AddFlight}></Route>
             <Route path="/updateflight/:id" component={UpdateFlight}></Route>
             <Route path="/updateroute/:id" component={UpdateRoute}></Route>
             <Route path="/displayalladmin" component={DisplayAllAdmin}></Route>
             <Route path="/displayallcustomer" component={DisplayAllCustomer}></Route>
             <Route path="/displayallroute" component={DisplayAllRoutes}></Route>
             <Route path="/displayallbooking" component={DisplayAllBooking}></Route>
             <Route path="/addroute" component={AddRoute}></Route>
             <Route path="/about" component={About}></Route>
             <Route path="/contact" component={Contact}></Route>
      </Switch>
     <FooterComponent /> 
     </Router>

    </div>
    
  );
}

export default App;
